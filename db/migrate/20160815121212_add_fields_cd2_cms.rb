class AddFieldsCd2Cms < ActiveRecord::Migration[5.0]
  def change

    add_column :cd2_cms_projects, :keypoints, :text
    add_column :cd2_cms_pages, :video_url, :string

  end
end

class AddFieldsToCd2Cms < ActiveRecord::Migration[5.0]
  def change
    add_column :cd2_cms_enquiries, :phone, :string
    add_column :cd2_cms_enquiries, :project_id, :integer


    add_column :cd2_cms_projects, :google_map, :text
  end
end

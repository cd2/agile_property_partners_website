class AddTextToCd2CmsImages < ActiveRecord::Migration[5.0]
  def change
    add_column :cd2_cms_images, :body, :text
    add_column :cd2_cms_images, :slideshow_image, :boolean
  end
end

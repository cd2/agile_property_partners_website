class AddStatusToProject < ActiveRecord::Migration[5.0]
  def change
    add_column :cd2_cms_projects, :status, :integer
  end
end

class AddAdminOptionsToCd2CmsPages < ActiveRecord::Migration[5.0]
  def change
    add_column :cd2_cms_pages, :image_text, :boolean
    add_column :cd2_cms_pages, :image_slideshow, :boolean
  end
end

task seed_pages: :environment do

  term = Cd2Cms::Page.create!(
      name: 'Terms and Conditions',
      body: seed_html('terms-and-conditions')
  )
  term.page_info.update(title: 'Terms and Conditions')

  contact = Cd2Cms::Page.create!(
      name: 'Contact',
      body: seed_html('contact-us'),
      in_menu: true
  )
  contact.page_info.update(protected: true, title: 'Contact Us', order: 7)

  projects= Cd2Cms::Page.create!(
      name: 'Projects',
      body: seed_html('projects'),
      in_menu: true
  )
  projects.page_info.update(protected: true, title: 'Our Projects', order: 3)
  projects.create_feature_image(image: seed_image('projects.jpg'))

  blog = Cd2Cms::Page.create!(
      name: 'Blog',
      body: seed_html('blog'),
      in_menu: true
  )
  blog.page_info.update(url_alias: 'blogs', protected: true, title: 'Blog', order: 5)
  blog.create_feature_image(image: seed_image('blog.jpg'))

  home = Cd2Cms::Page.create!(
    name: 'Home',
    layout: 'home',
    body: seed_html('home'),
    in_menu: true
  )
  home.page_info.update(protected: true, home_page: true, title: '', order: 1)
  home.images.create(image: seed_image('home.jpg'))

  @team = Cd2Cms::Page.create!(
      name: 'Norwich',
      body: seed_html('norwich'),
      in_menu: true,
      layout: 'slideshow'
  )
  @team.page_info.update(title: 'Norwich', order: 6)
  Dir.glob("#{Rails.root}/seeds/images/norwich/*") do |photo|
    @team.images.create(image: File.open(photo.to_s))
  end

  investors = Cd2Cms::Page.create!(
      name: 'Investors',
      body: seed_html('investors'),
      in_menu: true
  )
  investors.page_info.update(title: 'Investors', order: 4)
  investors.create_feature_image(image: seed_image('investors.jpg'))

  about = Cd2Cms::Page.create!(
      name: 'About Us',
      layout: 'about',
      body: seed_html('about-us'),
      in_menu: true
  )
  about.page_info.update(title: '', order: 2)
  about.create_feature_image(image: seed_image('about-us.jpg'))
  about.images.create(image: seed_image('richard.jpg'), caption: 'Richard', body: '<p>Alongside developing our investor relationships I’m kept busy sourcing new investment opportunities.</p>')
  about.images.create(image: seed_image('wayne.jpg'), caption: 'Wayne', body: '<p>I spend my time sourcing new investment opportunities and project managing them from start to finish once they are secured. </p>')


  proj1 = Cd2Cms::Project.create!(
      name: '2 Bed Cottage North Lopham',
      body: seed_html('2-bed-cottage-north-lopham')
  )
  proj1.images.create(image: seed_image('2-bed-cottage-north-lopham.jpg'))
  proj1.page_info.update(title: '2 Bed Cottage North Lopham')


  proj2 = Cd2Cms::Project.create!(
      name: '3 Bedroom Cottage, North Lopham',
      body: seed_html('3-bedroom-cottage-north-lopham')
  )
  proj2.images.create(image: seed_image('3-bedroom-cottage-north-lopham.jpg'))
  proj2.page_info.update(title: '3 Bedroom Cottage, North Lopham')

  proj3 = Cd2Cms::Project.create!(
      name: 'Coach House, North Lopham',
      body: seed_html('coach-house-north-lopham')
  )
  proj3.images.create(image: seed_image('coach-house-north-lopham.jpg'))
  proj3.page_info.update(title: 'Coach House, North Lopham')

  proj4 = Cd2Cms::Project.create!(
      name: 'HMO near University of East Anglia',
      body: seed_html('hmo-near-university-of-east-anglia')
  )
  proj4.images.create(image: seed_image('hmo-near-university-of-east-anglia.jpg'))
  proj4.page_info.update(title: 'HMO near University of East Anglia')

  proj5 = Cd2Cms::Project.create!(
      name: '3 Bed Bungalow Hellesdon, Norwich',
      body: seed_html('3-bed-bungalow-hellesdon-norwich')
  )
  proj5.images.create(image: seed_image('3-bed-bungalow-hellesdon-norwich.jpg'))
  proj5.page_info.update(title: '3 Bed Bungalow Hellesdon, Norwich')

  blog1 = Cd2Cms::Blog.create!(
      name: 'Norfolk - the perfect property investment place?',
      body: seed_html('norfolk-the-perfect-property-investment-place')
  )
  blog1.images.create!(image: seed_image('norfolk-the-perfect-property-investment-place.jpg'))
  blog1.page_info.update(title: 'Norfolk - the perfect property investment place?')

  blog2 = Cd2Cms::Blog.create!(
      name: 'Brexit - what you need to know about it',
      body: seed_html('brexit-what-you-need-to-know-about-it'),
      created_at: Time.now - 1.week
  )
  blog2.images.create!(image: seed_image('brexit-what-you-need-to-know-about-it.jpg'))
  blog2.page_info.update(title: 'Brexit - what you need to know about it')

  blog3 = Cd2Cms::Blog.create!(
      name: 'How Will Brexit Affect Property Prices and Property Investments?',
      body: seed_html('islands-in-the-stream-that-is-what-we-are'),
      created_at: Time.now - 2.weeks
  )
  blog3.images.create!(image: seed_image('islands-in-the-stream-that-is-what-we-are.jpg'))
  blog3.page_info.update(title: 'How Will Brexit Affect Property Prices and Property Investments?')

  blog4 = Cd2Cms::Blog.create!(
      name: 'How to get your estate agent working for you and your investment.',
      body: seed_html('how-to-get-your-estate-agent-working-for-you-and-your-investment'),
      created_at: Time.now - 3.weeks
  )
  blog4.images.create!(image: seed_image('how-to-get-your-estate-agent-working-for-you-and-your-investment.jpg'))
  blog4.page_info.update(title: 'How to get your estate agent working for you and your investment.')

  blog5 = Cd2Cms::Blog.create!(
      name: 'One Of The Hottest Property Markets In The UK?',
      body: seed_html('one-of-the-hottest-property-markets-in-the-uk'),
      created_at: Time.now - 4.weeks
  )
  blog5.images.create!(image: seed_image('one-of-the-hottest-property-markets-in-the-uk.jpg'))
  blog5.page_info.update(title: 'One Of The Hottest Property Markets In The UK?')

  blog6 = Cd2Cms::Blog.create!(
      name: 'Lunch On The Rooftop?',
      body: seed_html('lunch-on-the-rooftop'),
      created_at: Time.now - 5.weeks
  )
  blog6.images.create!(image: seed_image('lunch-on-the-rooftop.jpg'))
  blog6.page_info.update(title: 'Lunch On The Rooftop?')

  blog7 = Cd2Cms::Blog.create!(
      name: 'North Lopham: Case Study Of A Property Deal',
      body: seed_html('north-lopham-case-study-of-a-property-deal'),
      created_at: Time.now - 6.weeks
  )
  blog7.images.create!(image: seed_image('north-lopham-case-study-of-a-property-deal.jpg'))
  blog7.page_info.update(title: 'North Lopham: Case Study Of A Property Deal')

end




def seed_html html_file
  File.open(Rails.root + "seeds/html/#{html_file}.html").read
end

def seed_image image
  File.open("#{Rails.root}/seeds/images/#{image}")
end
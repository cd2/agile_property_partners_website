task seed_projects: :environment do


  proj1 = Cd2Cms::Project.create!(
      name: '2 Bed Cottage North Lopham',
      body: seed_html('2-bed-cottage-north-lopham')
  )
  proj1.images.create(image: seed_image('2-bed-cottage-north-lopham.jpg'))
  proj1.page_info.update(title: '2 Bed Cottage North Lopham')


  proj2 = Cd2Cms::Project.create!(
      name: '3 Bedroom Cottage, North Lopham',
      body: seed_html('3-bedroom-cottage-north-lopham')
  )
  proj2.images.create(image: seed_image('3-bedroom-cottage-north-lopham.jpg'))
  proj2.page_info.update(title: '3 Bedroom Cottage, North Lopham')

  proj3 = Cd2Cms::Project.create!(
      name: 'Coach House, North Lopham',
      body: seed_html('coach-house-north-lopham')
  )
  proj3.images.create(image: seed_image('coach-house-north-lopham.jpg'))
  proj3.page_info.update(title: 'Coach House, North Lopham')

  proj4 = Cd2Cms::Project.create!(
      name: 'HMO near University of East Anglia',
      body: seed_html('hmo-near-university-of-east-anglia')
  )
  proj4.images.create(image: seed_image('hmo-near-university-of-east-anglia.jpg'))
  proj4.page_info.update(title: 'HMO near University of East Anglia')

  proj5 = Cd2Cms::Project.create!(
      name: '3 Bed Bungalow Hellesdon, Norwich',
      body: seed_html('3-bed-bungalow-hellesdon-norwich')
  )
  proj5.images.create(image: seed_image('3-bed-bungalow-hellesdon-norwich.jpg'))
  proj5.page_info.update(title: '3 Bed Bungalow Hellesdon, Norwich')

end

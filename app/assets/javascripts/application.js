// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-unslider.min
//= require social-share-button
//= require lightbox
//= require_tree .
$(function() {
    $('.flexslider').unslider({
        animation: "horizontal",
        speed: 700,
        delay: 7000,
        infinite: true,
        autoplay: true,
        easing: 'swing',
        arrows: false
    });
    lightbox.init()
    
    $('#menu_toggle').on('click', function(){
        $('body').toggleClass('menu_open');
    });


    $('[data-filter]').on('click', function(e){
        $('[data-filter]').removeClass('active')
        $(this).addClass('active')
        var filter = $(this).data('filter')
        if (filter === 'all') {
            $('[data-status]').show();
        } else {
           $('[data-status]').hide();
           $('[data-status=' + filter + ']').show()
        }
        e.preventDefault();
    });

});

lightbox.option({
  fadeDuration:100,
  resizeDuration:100,
  wrapAround:!0
});


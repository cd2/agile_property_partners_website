Cd2Cms::ProjectsController.class_eval do

  def index
    @q = @resource_driver.ransack(params[:q])
    @objects = @q.result(distinct: true)
  end

  def sort
    params[:project].each_with_index do |id, index|
      Cd2Cms::Project.find_by(id:id).page_info.update({order: index+1})
    end
    render nothing: true
  end

  private

  def set_links
    @index_path = projects_path
    @new_path = new_project_path
    @sortable = sort_projects_path
  end

end

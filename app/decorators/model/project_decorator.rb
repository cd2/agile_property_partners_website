Cd2Cms::Project.class_eval do

  self.permitted_params = [:status, :keypoints, :google_map]

  enum status: [:available_now, :coming_soon, :completed]

end

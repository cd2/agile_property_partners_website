Cd2Cms::Page.class_eval do

  self.permitted_params = [:image_text, :image_slideshow, :video_url]

end

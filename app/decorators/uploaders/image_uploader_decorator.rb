Cd2Cms::ImageUploader.class_eval do

  version :team do
    process :resize_to_fill => [300, 200]
  end

  version :slideshow do
    process :resize_to_fill => [1200, 500]
  end

  version :short_banner do
    process :resize_to_fill => [1200, 350]
  end

  version :home_slideshow do
    process :resize_to_fill => [1600, 600]
  end


end
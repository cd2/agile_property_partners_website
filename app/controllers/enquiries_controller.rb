class EnquiriesController < ApplicationController
  before_action :set_enquiry_page

  def new
    @enquiry = Cd2Cms::Enquiry.new
  end

  def create
    @enquiry = Cd2Cms::Enquiry.new(enquiry_params)
    if @enquiry.save
      EnquiriesMailer.send_enquiry(@enquiry).deliver_now
      redirect_to thanks_enquiries_path
    else
      render :new
    end
  end

  def thanks
  end

  private

    def set_enquiry_page
      @page = Cd2Cms::Page.get_from_url('contact')
      @page_info = @page.page_info
    end

    def enquiry_params
      params.require(:enquiry).permit(:name, :email, :body, :phone, :project_id)
    end


end

class BlogsController < ApplicationController

  def index
    @page = Cd2Cms::Page.get_from_url('blogs')
    @page_info = @page.page_info
  end

  def show
    @blog = Cd2Cms::Blog.get_from_url(params[:id])
  end

end

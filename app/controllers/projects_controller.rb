class ProjectsController < ApplicationController
  def index
    @page = Cd2Cms::Page.get_from_url('projects')
    @page_info = @page.page_info
    @projects = Cd2Cms::Project.includes(:page_info).where(cd2_cms_page_infos: {published: true})
  end

  def show
    @project = Cd2Cms::Project.get_from_url(params[:id])
    @page_info = @project.page_info
  end
end

class EnquiriesMailer < ApplicationMailer
  def send_enquiry enquiry
    @enquiry = enquiry
    mail to: 'info@a-pp.co.uk', subject: "Website Enquiry", from: @enquiry.email
  end
end
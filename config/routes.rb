Cd2Cms::Engine.routes.draw do

  resources :projects do
    collection { post :sort }
  end

end

Rails.application.routes.draw do
get '/blog/history-of-property-market-in-norfolk-from-recession-ripple-effect-to-bouncing-back', to: redirect('/blogs/history-of-property-market-in-norfolk-from-recession-ripple-effect-to-bouncing-back', status: 301)
get '/blog/how-inflation-can-affect-property-investment-a-7-point-guide', to: redirect('/blogs/how-inflation-can-affect-property-investment-a-7-point-guide', status: 301)
get '/blog/sdlt-multiple-dwelling-relief-loophole-explained', to: redirect('/blogs/sdlt-multiple-dwelling-relief-loophole-explained', status: 301)
get '/blog/setting-and-reaching-goals-when-investing', to: redirect('/blogs/setting-and-reaching-goals-when-investing', status: 301)
get '/blog/norfolk-the-perfect-property-investment-place', to: redirect('/blogs/norfolk-the-perfect-property-investment-place', status: 301)
get '/blog/6', to: redirect('/blogs/6', status: 301)
get '/blog/islands-in-the-stream-that-is-what-we-are', to: redirect('/blogs/islands-in-the-stream-that-is-what-we-are', status: 301)
get '/blog/how-to-get-your-estate-agent-working-for-you-and-your-investment', to: redirect('/blogs/how-to-get-your-estate-agent-working-for-you-and-your-investment', status: 301)
get '/blog/one-of-the-hottest-property-markets-in-the-uk', to: redirect('/blogs/one-of-the-hottest-property-markets-in-the-uk', status: 301)
get '/blog/lunch-on-the-rooftop', to: redirect('/blogs/lunch-on-the-rooftop', status: 301)
get '/blog/1', to: redirect('/blogs/1', status: 301)
  get '/blog/', to: redirect('/blogs', status: 301)

  get '/Team', to: redirect('/about-us', status: 301)
  get '/about', to: redirect('/about-us', status: 301)

  mount Cd2Cms::Engine => '/admin'

  scope module: :cd2_cms do
    get '/sitemap', defaults: { :format => :xml }, to: 'sitemap#show'
  end

  resources :enquiries, only: [:new, :create], path: 'contact', path_names: {new: '/'} do
    get 'thanks', on: :collection
  end
  resources :blogs, only: [:index, :show]
  resources :projects, only: [:index, :show]

  resources :pages, only: :show, path: ''
  root 'pages#show'

end
